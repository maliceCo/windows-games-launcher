﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace launcher
{
    class Database
    {
        private SQLiteCommand cmd;
        SQLiteConnection conn;

        public void InitConnection()
        {
            conn = new SQLiteConnection(@"Data Source=troniksdb.sqlite");
            conn.Open();
        }

        public void InsertNew(string juego)
        {
            cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO Juegos (juego, fecha) VALUES (@juego, @fecha);";

            cmd.Parameters.AddWithValue("@juego", juego);
            cmd.Parameters.AddWithValue("@fecha", DateTime.Now);

            cmd.ExecuteNonQuery();
        }

        private string DateTimeSQLite(DateTime datetime)
        {
            return string.Format("{0:yyyy/MM/dd HH:mm:ss}", datetime);
        }
    }
}
