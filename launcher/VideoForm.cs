﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;

namespace launcher
{
    public partial class VideoForm : Form
    {
        WMPLib.WindowsMediaPlayer Player;

        public VideoForm()
        {
            InitializeComponent();

            axWindowsMediaPlayer1.URL = @"C:\AppDatat\knino.mp4";
            axWindowsMediaPlayer1.uiMode = "none";
            //axWindowsMediaPlayer1.Ctlcontrols.play();

            //PlayFile(@"C:\AppDatat\knino_xvid.avi");
        }

        private void PlayFile(String url)
        {
            Player = new WMPLib.WindowsMediaPlayer();
            Player.PlayStateChange +=
                new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(Player_PlayStateChange);
            Player.MediaError +=
                new WMPLib._WMPOCXEvents_MediaErrorEventHandler(Player_MediaError);
            
            Player.URL = url;
            Player.uiMode = "none";
            Player.controls.play();
        }

        private void Player_PlayStateChange(int NewState)
        {
            if ((WMPLib.WMPPlayState)NewState == WMPLib.WMPPlayState.wmppsStopped)
            {
                this.Close();
            }
        }

        private void Player_MediaError(object pMediaObject)
        {
            MessageBox.Show("Cannot play media file.");
            this.Close();
        }
    }
}