﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace launcher
{
    public partial class Form1 : Form
    {
        private Database database;
        private Process process;

        public Form1()
        {
            database = new Database();
            database.InitConnection();
            InitializeComponent();
            axWindowsMediaPlayer1.URL = @"C:\AppDatat\knino.mp4";
            axWindowsMediaPlayer1.uiMode = "none";

            process = new Process();
            process.EnableRaisingEvents = true;
            process.Exited += Form1_Exited;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            database.InsertNew("corrupto");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\kaninocorrer.exe";
            //startInfo.FileName = @"C:\AppDatat\castigacorruptoParaMenu.exe";
            StartGame(startInfo);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            database.InsertNew("soccer");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\pilsencupo.exe";
            //startInfo.FileName = @"C:\AppDatat\SoccerTroniksGameLand.exe";
            StartGame(startInfo);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            database.InsertNew("musicBrain");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\musicBrain.exe";
            StartGame(startInfo);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            database.InsertNew("spaceShooterCoop");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\SpaceShooterCooperative.exe";
            StartGame(startInfo);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            database.InsertNew("spaceShooterCPU");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\SpaceShooterP1vsCpu_Landscape.exe";
            StartGame(startInfo);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            database.InsertNew("hockey");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\hockey.exe";
            StartGame(startInfo);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            database.InsertNew("spaceShooter");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"C:\AppDatat\spaceshooter.exe";
            StartGame(startInfo);
        }

        private void StartGame(ProcessStartInfo startInfo)
        {
            axWindowsMediaPlayer1.Ctlcontrols.stop();
            process.StartInfo = startInfo;
            process.Start();
        }

        private void Form1_Exited(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.Ctlcontrols.play();
        }
    }
}